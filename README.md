# AsteriosPHP

* Version: 2.0.0
* [Website](https://asteriosphp.de)

## Description

AsteriosPHP a free and simple PHP web framework. It is extremely portable, works on almost any server and provide a clean and readable syntax.
he target is that you also can use parts of this framework without installing the whole framework.

AsteriosPHP is fully PHP 8 compatible.

Here some key facts about AsteriosPHP:
- PSR-4 Autoloader
- Own namespace
- Easy to configurate
- Easy to use
